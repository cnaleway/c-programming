#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>


/*
 * Run this on the cs server. It connects to itself, port 25, and send an email
 * using SMTP. All the addresses except the recipient are fake.
 *
 * We need to use the cs server because it allows relaying as long as the client
 * is on the local LAN.
 */

void error(char *);

int main(int argc, char **argv)
{
    struct sockaddr_in sa;
    int sockfd;

    //if (argc < 3)
    //{
    //    fprintf(stderr, "Usage: %s recipient \"message\"\n", argv[0]);
//      exit(1);
 //   }

    sa.sin_family = AF_INET;
    sa.sin_port = htons(1234);
    inet_pton(AF_INET, "192.155.81.33", &sa.sin_addr);

    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1)
    {
        fprintf(stderr, "Can't create socket\n");
        exit(3);
    }
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1)
    {
        fprintf(stderr, "Can't connect\n");
        exit(2);
    }

    char buf[1000];
    size_t rsize;
    char resultcode[1000];

    // lOGIN
    sprintf(buf, "LOGIN Eating\n");
    send(sockfd, buf, strlen(buf), 0);

    // Expecting +ok
    rsize = recv(sockfd, buf, 1000, 0);
    sscanf(buf, "%[^ ] ", resultcode);
    if (strcmp(resultcode, "+OK") != 0) error("Didn't get +ok for result code");

    // lIST
    sprintf(buf, "LIST\n");
    send(sockfd, buf, strlen(buf), 0);

    // Expecting +ok
    rsize = recv(sockfd, buf, 1000, 0);
    printf("%s\n", buf);
    sscanf(buf, "%[^ ]", resultcode);
    if (strcmp(resultcode, "+OK") != 0) error("Didn't get +ok for result code");


    while (1)
    {
	char *buffer;
	buffer = readline("Jo Jo: ");
        if (!strcmp(buffer, "/list"))
        {
        	sprintf(buf, "LIST\n");
        	send(sockfd, buf, strlen(buf), 0);
        	rsize = recv(sockfd, buf, 1000, 0);
        	sscanf(buf, "%[^ ]", resultcode);
		printf("%d\n",strcmp(resultcode, "+OK"));
		printf("%s\n", buf);
        	if (!strcmp(resultcode, "+OK") != 0) error("Didn't get +ok for result code");

        	   free (buffer);
        }

        else if (!strcmp(buffer, "/quit"))
        {
        	sprintf(buf, "QUIT\n");
       		send(sockfd, buf, strlen(buf), 0);
        	rsize = recv(sockfd, buf, 1000, 0);
        	sscanf(buf, "%[^ ]", resultcode);
 	        free (buffer);
        	break;
		send(sockfd, buf, strlen(buf), 0);
		close(sockfd);

        }
 	else
	{
		sprintf(buf, "SEND: %s\n", buffer);
       		send(sockfd, buf, strlen(buf), 0);
        	rsize = recv(sockfd, buf, 1000, 0);
        	sscanf(buf, "%[^ ]", resultcode);
	   	if (!strcmp(resultcode, "+OK")) error("Didn't get +ok for result code");

		while(strcmp(resultcode, "+OK"))
		{
			if (!strcmp(resultcode, "-ERR")) break;
			sprintf(buf, "RECV\n");
                	send(sockfd, buf, strlen(buf), 0);
                	rsize = recv(sockfd, buf, 1000, 0);
	        	printf("%s\n", buf);
		}
	}
	}
    return 0;
}

void error(char *s)
{
    fprintf(stderr, "%s\n", s);
    exit(1);
}
